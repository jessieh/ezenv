;; ezenv.fnl
;; Provides an easy way to load configuration profiles from environment variables

;; -------------------------------------------------------------------------- ;;
;; License

;; Copyright (C) 2021 Jessie Hildebrandt

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; -------------------------------------------------------------------------- ;;
;; Helper functions

;; ---------------------------------- ;;
;; Nice error messages

(fn error_message [...]
  (error (accumulate [message ""
                      _ value (ipairs [...])]
           (let [value_string (tostring value)]
             (.. message value_string)))))

;; ---------------------------------- ;;
;; Assertion

(fn assert_properties_supported [properties]
  "Assert that all property keys specified in `properties` are supported."
  (each [property_name _ (pairs properties)]
    (match property_name
      "type" true
      "required" true
      "default" true
      _ (error_message "ezenv error: Unsupported variable property specified in config structure.
 - Supported properties: type, required, default
 -   Specified property: " property_name))))

(fn assert_type_supported [type_string]
  "Assert that `type_string` references a supported value type."
  (match type_string
    "string" true
    "number" true
    "boolean" true
    _ (error_message "ezenv error: Unsupported value type specified in config structure.
 - Supported types: string, number, boolean
 -  Specified type: " type_string)))

(fn assert_default_type [properties]
  "Assert that if `properties` specifies `default`, that `properties.default` is of type `properties.type`."
  (when (and (~= properties.default nil)
             (~= (type properties.default) properties.type))
    (error_message "ezenv error: Specified default value does not match specified type.
 - Specified default: " properties.default "
 -   Type of default: " (type properties.default) "
 -    Specified type: " properties.type)))

(fn assert_value_exists [value]
  "Assert that `value` is non-nil."
  (when (= value nil)
    (error_message "Configuration error: Required value not provided.")))

(fn assert_value_type [value type_string]
  "Assert that `value` is of type `type_string`."
  (when (not (= (type value) type_string))
    (error_message "Configuration error: Type conversion failed for a value in your configuration.
 -  Expected type: " type_string "
 - Provided value: " value)))

(fn check_asserts [env_value value properties]
  "Check provided `env_value`, `value`, and `properties` against all supported assertions and tests."
  (assert_properties_supported properties)
  (assert_type_supported properties.type)
  (assert_default_type properties)
  (when properties.required
    (if env_value
      (assert_value_type value properties.type)
      (assert_value_exists value))))

;; ---------------------------------- ;;
;; Conversion

(fn string_to_boolean [string]
  "Return `string` after converting it into a boolean.
Returns `string` as a boolean if it contains a boolean value, otherwise returns nil."
  (match (string.lower string)
    "true" true
    "false" false
    _ nil))

(fn convert_string_to_type [string type_string]
  "Return the value of `string` after conversion into type `type_string`."
  (match type_string
    "string" string
    "number" (tonumber string)
    "boolean" (string_to_boolean string)
    _ nil))

;; -------------------------------------------------------------------------- ;;
;; Module functions

(fn load [config_structure]
  "Return table of environment variable values according to `config_structure`."
  (collect [name properties (pairs config_structure)]
    (do
      (when (not properties.type)
        (tset properties :type "string"))
      (when (~= properties.default nil)
        (tset properties :required true))
      (let [env_value (os.getenv name)
            value (if env_value
                      (convert_string_to_type env_value properties.type)
                      properties.default)
            (ok? err) (pcall #(check_asserts env_value value properties))]
        (when (not ok?)
          (error_message "Error in configuration for value '" name "':\n" err))
        (values name value)))))

;; -------------------------------------------------------------------------- ;;
;; Provide ezenv module

{: load}
