-- spec/load_spec.lua
-- Specifies tests for ezenv.load functionality

--------------------------------------------------------------------------------
-- Module imports

ezenv = require( 'ezenv' )

--------------------------------------------------------------------------------
-- Test context

describe( 'ezenv.load functionality', function()

             ----------------------------------------
             -- Environment variables table

             local env_vars

             ----------------------------------------
             -- Pre-test setup

             before_each( function()
                   env_vars = {}
             end )

             ----------------------------------------
             -- Replace os.getenv with mock function

             setup( function()

                   _G.os.getenv = function( name )

                      local env_var = env_vars[ name ]

                      if ( type( env_var ) ~= 'nil' and type( env_var ) ~= 'string' )
                      then
                         error( 'Malformed test: please ensure that all values in env_vars are of type "string" or "nil".' )
                      end

                      return env_var

                   end

             end )

             ----------------------------------------
             -- Mock os.getenv sanity check

             describe( 'os.getenv mock function sanity check', function()
                          it( 'ensures that all values in env_vars are of type "string" or "nil"', function ()
                                 env_vars = {
                                    some_num = '2',
                                    another_num = 2
                                 }
                                 assert.has_no.errors( function() os.getenv( 'some_num' ) end )
                                 assert.has_error( function() os.getenv( 'another_num' ) end )
                          end )
             end )

             ----------------------------------------
             -- Test definitions

             it( 'provides environment variables', function()

                    env_vars = {
                       name = 'jeff',
                       some_num = '2',
                       some_bool = 'true'
                    }

                    local expected_values = {
                       name = 'jeff',
                       some_num = '2',
                       some_bool = 'true',
                       not_assigned = nil
                    }

                    local config_structure = {
                       name = {},
                       some_num = {},
                       some_bool = {},
                       not_assigned = {}
                    }

                    assert.are.same( expected_values, ezenv.load( config_structure ) )

             end )

             it( 'provides environment variables with type conversion', function()

                    env_vars = {
                       name = 'jeff',
                       some_num = '2',
                       some_bool = 'true',
                    }

                    local expected_values = {
                       name = 'jeff',
                       some_num = 2,
                       some_bool = true,
                       not_assigned = nil
                    }

                    local config_structure = {
                       name = { type = 'string' },
                       some_num = { type = 'number' },
                       some_bool = { type = 'boolean' },
                       not_assigned = { type = 'string' }
                    }

                    assert.are.same( expected_values, ezenv.load( config_structure ) )

             end )

             it( 'provides fallback default values for unassigned variables', function()

                    local expected_values = {
                       name = 'jeff',
                       last_name = 'smith',
                       age = 30
                    }

                    local config_structure = {
                       name = { type = 'string', default = 'jeff' },
                       last_name = { type = 'string', default = 'smith' },
                       age = { type = 'number', default = 30 }
                    }

                    assert.are.same( expected_values, ezenv.load( config_structure ) )

             end )

             it( 'overrides default values when variables are asssigned', function()

                    env_vars = {
                       name = 'joseph',
                       last_name = 'johnson',
                       age = '40'
                    }

                    local expected_values = {
                       name = 'joseph',
                       last_name = 'johnson',
                       age = 40
                    }

                    local config_structure = {
                       name = { type = 'string', default = 'jeff' },
                       last_name = { type = 'string', default = 'smith' },
                       age = { type = 'number', default = 30 }
                    }

                    assert.are.same( expected_values, ezenv.load( config_structure ) )

             end )

             it( 'provides "nil" for non-required typed variables whose assigned values fail type conversion', function()

                    env_vars = {
                       some_num = 'uh oh!',
                       some_bool = 'uh oh!'
                    }

                    local expected_values = {
                       some_num = nil,
                       some_bool = nil
                    }

                    local config_structure = {
                       some_num = { type = 'number' },
                       some_bool = { type = 'boolean' }
                    }

                    assert.are.same( expected_values, ezenv.load( config_structure ) )

             end )

             it( 'asserts that the values of required variables are non-nil', function()

                    local config_structure = {
                       required_string = { required = true },
                    }

                    assert.has_error( function() ezenv.load( config_structure ) end )

                    local str_config_structure = {
                       required_str = { type = 'string', required = true },
                    }

                    assert.has_error( function() ezenv.load( typed_config_structure ) end )


                    local num_config_structure = {
                       required_num = { type = 'number', required = true },
                    }

                    assert.has_error( function() ezenv.load( typed_config_structure ) end )

                    local bool_config_structure = {
                       required_bool = { type = 'boolean', required = true },
                    }

                    assert.has_error( function() ezenv.load( typed_config_structure ) end )

             end )

             it( 'treats "default" as implying "required"', function()

                    env_vars = {
                       required_num = "uh oh!"
                    }

                    local config_structure = {
                       required_num = { type = 'number', default = 123 }
                    }

                    assert.has_error( function() ezenv.load( config_structure ) end )

             end )

             it( 'asserts that required environment variable types are correct', function()

                    env_vars = {
                       should_be_num = 'this isnt a number',
                    }

                    local num_config_structure = {
                       should_be_num = { type = 'number', required = true }
                    }

                    assert.has_error( function() ezenv.load( num_config_structure ) end )

                    env_vars = {
                       should_be_bool = 123
                    }

                    local bool_config_structure = {
                       should_be_bool = { type = 'bool', required = true }
                    }

                    assert.has_error( function() ezenv.load( config_structure ) end )

             end )

             it( 'asserts that default values are not used even if a value of the incorrect type is assigned', function()

                    env_vars = {
                       should_be_num = 'uh oh!'
                    }

                    local config_structure = {
                       should_be_num = { type = 'number', default = 123 }
                    }

                    assert.has_error( function() ezenv.load( config_structure ) end )

             end )

             it( 'asserts that the types of provided default values are correct', function()

                    local valid_config_structure = {
                       some_num = { type = 'number', default = 123 },
                       some_bool = { type = 'boolean', default = true }
                    }

                    assert.has_no.errors( function() ezenv.load( valid_config_structure ) end )

                    local invalid_str_config_structure = {
                       should_be_str = { type = 'string', default = 123 },
                    }

                    assert.has_error( function() ezenv.load( invalid_str_config_structure ) end )

                    local invalid_num_config_structure = {
                       should_be_num = { type = 'number', default = false }
                    }

                    assert.has_error( function() ezenv.load( invalid_num_config_structure ) end )

                    local invalid_bool_config_structure = {
                       should_be_bool = { type = 'boolean', default = 'uh oh!' }
                    }

                    assert.has_error( function() ezenv.load( invalid_bool_config_structure ) end )

             end )

             it( 'asserts that type conversion is not performed on default values', function()

                    local invalid_num_config_structure = {
                       should_be_num = { type = 'number', default = '123' }
                    }

                    assert.has_error( function() ezenv.load( invalid_num_config_structure ) end )

                    local invalid_bool_config_structure = {
                       should_be_bool = { type = 'boolean', default = 'true' }
                    }

                    assert.has_error( function() ezenv.load( invalid_bool_config_structure ) end )

             end )

             it( 'asserts that all specified environment variable properties are supported', function ()

                    local config_structure = {
                       some_num = { type = 'number', default = '0', unknown_property = 'hmm...' }
                    }

                    assert.has_error( function() ezenv.load( config_structure ) end )

             end )

             it( 'asserts that all specified environment variable types are supported', function()

                    local valid_config_structure = {
                       some_str = { type = 'string' },
                       some_num = { type = 'number' },
                       some_bool = { type = 'boolean' }
                    }

                    assert.has_no.errors( function() ezenv.load( valid_config_structure ) end )

                    local invalid_config_structure = {
                       invalid_type = { type = 'not a type!' },
                    }

                    assert.has_error( function() ezenv.load( invalid_config_structure ) end )

             end )

             it( 'asserts that boolean types are properly converted from strings (case-insensitive)', function()

                    env_vars = {
                       true_lowercase = 'true',
                       true_uppercase = 'TRUE',
                       true_mixedcase = 'True',
                       false_lowercase = 'false',
                       false_uppercase = 'FALSE',
                       false_mixedcase = 'False'
                    }

                    local config_structure = {
                       true_lowercase = { type = 'boolean' },
                       true_uppercase = { type = 'boolean' },
                       true_mixedcase = { type = 'boolean' },
                       false_lowercase = { type = 'boolean' },
                       false_uppercase = { type = 'boolean' },
                       false_mixedcase = { type = 'boolean' }
                    }

                    local expected_values = {
                       true_lowercase = true,
                       true_uppercase = true,
                       true_mixedcase = true,
                       false_lowercase = false,
                       false_uppercase = false,
                       false_mixedcase = false
                    }

                    assert.are.same( expected_values, ezenv.load( config_structure ) )

             end )

             it( 'asserts that variable properties are provided as tables', function()

                    local config_structure = {
                       not_valid = 'uh oh!'
                    }

                    assert.has_error( function() ezenv.load( config_structure) end )

             end )

             it( 'assumes that variables are strings if not otherwise specified', function()

                    local valid_config_structure = {
                       should_be_str = { default = 'abc' }
                    }

                    assert.has_no.errors( function() ezenv.load( valid_config_structure ) end )

                    local invalid_config_structure = {
                       should_be_str = { default = 123 }
                    }

                    assert.has_error( function() ezenv.load( invalid_config_structure ) end )

             end )

end )
