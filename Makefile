# ezenv Makefile
# Produces .lua files from the .fnl source files

SRC_DIR = src
BUILD_DIR = build

FENNEL = ./bin/fennel-0.10.0-x86_64

fnl_files := $(shell find $(SRC_DIR)/ -type f -name '*.fnl')
lua_files := $(patsubst $(SRC_DIR)/%.fnl, $(BUILD_DIR)/%.lua, $(fnl_files))

.PHONY: install clean

all: $(lua_files)

$(BUILD_DIR)/%.lua: $(SRC_DIR)/%.fnl
	mkdir -p "$(@D)"
	$(FENNEL) --compile $< > $@

install:
	@echo "Leaving installation up to luarocks..."

clean:
	rm -rf $(BUILD_DIR)
	rm -f *.src.rock
