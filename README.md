# <img src="../../raw/assets/icon.png" width=75> ezenv

Easily load configuration profiles from environment variables in Lua.

Written in [Fennel](https://fennel-lang.org).

[![Test Status](https://gitlab.com/jessieh/ezenv/badges/main/pipeline.svg)](https://gitlab.com/jessieh/ezenv/-/pipelines) [![Code Coverage](https://gitlab.com/jessieh/ezenv/badges/main/coverage.svg)](https://gitlab.com/jessieh/ezenv/-/jobs)

## Table of Contents

 - [Installation](#installation)
 - [Example Usage](#example-usage)
 - [Docker Usage](#docker-usage)
 - [Documentation](#documentation)
   - [ezenv.load](#ezenvload)
   - [Config Structure](#config-structure)
   - [Variable Properties](#variable-properties)
     - [type](#type)
     - [default](#default)
     - [required](#required)
 - [Testing](#testing)

## Installation

Install with [luarocks](https://luarocks.org):

```bash
luarocks install ezenv
```

For a list of available versions, see the [luarocks module page](https://luarocks.org/modules/jessieh/ezenv).

## Example Usage

```lua
-- my_app.lua

ezenv = require 'ezenv'

config = ezenv.load( { name = { type = 'string', required = true }, age = { type = 'number' } } )

print( 'Hello, ' .. config.name .. '!' )
if config.age then
    print( 'Next year you will be ' .. ( config.age + 1 ) .. ' years old!' )
end
```

<img src="../../raw/assets/demo.png" width=600>

## Docker Usage

Services and applications that load their configuration from environment variables are easily configurable when Dockerized.

You can write an environment file containing your configuration:

```bash
# serviceconfig.env
listen_port=8008
environment=production
logging=true
```

...and then integrate it into your `docker run` command:

```bash
docker run --env-file serviceconfig.env -p 8008:8008 myluaservice:latest
```

...or your Compose file:

```yaml
# docker-compose.yml
version: '3'
services:
    myluaservice:
        image: 'myluaservice:latest'
        ports:
            - "8080:8080"
        env_file:
            - serviceconfig.env
```

## Documentation

### ezenv.load

```
ezenv.load( config_structure )
```
Return table of environment variable values according to `config_structure`.

### Config Structure

`config_structure` should be a table in which each element represents an environment variable to be loaded:

```lua
{
    variable_1 = { --[[ variable properties here ]] },
    variable_2 = { --[[ variable properties here ]] },
    -- etc.
}
```

### Variable Properties

The properties of each variable are defined as a table, with a number of supported properties:

#### type

If specified, the environment variable's value will be converted from a string into the specified type after loading:

```lua
{
    my_string = { type = 'string' },
    my_boolean = { type = 'boolean' },
    my_number = { type = 'number' }
}
```

If the environment variable's value could not be successfully converted into the expected type, its value will be `nil`.

Note that if `type` is not specified, it will default to `string`:

```lua
{
    is_a_string = {},
}
```

#### default

If specified, will act as a default value for the variable if one is not assigned:

```lua
{
    my_string = { default = 'abc' },
    my_boolean = { type = 'boolean', default = true },
    my_number = { type = 'number', default = 123 }
}
```

Notes:
 - An error will be thrown if the type of `default` does not match `type`.
 - Specifying `default` implies specifying `required`.
   - The reason for this is to avoid confusing or unexpected behavior when a user specifies a value with an invalid type:
   The alternative would be either to allow the variable's value to be `nil` after failing conversion, or to revert to using `default` instead of their value.
   It is best to present an error up front so that they may correct the value in their supplied configuration.

#### required

If `true`, will throw an error if a value is not provided for the variable:

```lua
{
    mandatory = { required = true }
}
```

## Testing

The Fennel code needs to be compiled before tests can be run:

```bash
# Build the project:
luarocks build

# Now you can run tests:
luarocks test
```
